import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flash_chat/widgets/message_bubble_builder.dart';

final FirebaseFirestore _fireStore = FirebaseFirestore.instance;

class MessageStreamBuilder extends StatelessWidget {
  const MessageStreamBuilder({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tmp = _fireStore.collection('messages').orderBy('createdAt', descending: true).snapshots();
    print(tmp);
    return StreamBuilder<QuerySnapshot>(
      stream: tmp,
      builder: (
        BuildContext context,
        AsyncSnapshot<QuerySnapshot> snapshot,
      ) {
        if (!snapshot.hasData) {
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: Colors.lightBlueAccent,
            ),
          );
        } else if (snapshot.hasError) {
          return const Center(child: Text('Something went wrong'));
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: Text('Loading data...'));
        }
        // final messagesList = snapshot.data!.docs;
        // final messageBubbleList = <MessageBubble>[];
        // for (var msg in messagesList) {}
        return Expanded(
          child: ListView(
            reverse: true,
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
            children: snapshot.data!.docs.map((DocumentSnapshot doc) {
              final data = doc.data()! as Map<String, dynamic>;

              return MessageBubble(
                sender: data['sender'],
                text: data['text'],
              );
            }).toList(),
          ),
        );
      },
    );
  }
}
