import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final Color? color;
  final String? title;
  final VoidCallback onPressed;

  const RoundedButton({
    this.color,
    this.title,
    required this.onPressed,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        color: color ?? Colors.pinkAccent,
        borderRadius: BorderRadius.circular(30.0),
        elevation: 5.0,
        child: MaterialButton(
          onPressed: onPressed,
          minWidth: 200.0,
          height: 42.0,
          child: Text(
            title ?? "ExampleTitle",
            style: const TextStyle(
              fontFamily: 'SourceCodePro',
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
