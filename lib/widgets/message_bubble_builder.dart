import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flash_chat/util/constants.dart';

class MessageBubble extends StatelessWidget {
  final String sender;
  final String text;
  late final bool test;

  MessageBubble({Key? key, required this.sender, required this.text}) : super(key: key) {
    test = (sender == FirebaseAuth.instance.currentUser!.email);
  }

  BorderRadiusGeometry resolveBubbleGeometry() {
    if (test) {
      return const BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
        bottomRight: Radius.circular(30.0),
      );
    } else {
      return const BorderRadius.only(
        topLeft: Radius.circular(30.0),
        topRight: Radius.circular(30.0),
        bottomLeft: Radius.circular(30.0),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(3.0),
      child: Column(
        crossAxisAlignment: test ? CrossAxisAlignment.start : CrossAxisAlignment.end,
        children: [
          Text(
            sender,
            style: const TextStyle(fontSize: 12.0, color: Colors.black54),
          ),
          Material(
            elevation: 5.0,
            borderRadius: resolveBubbleGeometry(),
            color: test ? kMainAccentColor : kSecondaryAccentColor,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                text,
                textAlign: TextAlign.right,
                style: const TextStyle(fontSize: 15.0, color: Colors.white),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
