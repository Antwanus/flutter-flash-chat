import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flash_chat/screens/chat_screen.dart';
import 'package:flutter_flash_chat/util/constants.dart';
import 'package:flutter_flash_chat/widgets/rounded_button.dart';

class LoginScreen extends StatefulWidget {
  static const String routeId = 'login';

  const LoginScreen({Key? key}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late String email;
  late String password;
  bool isSpinnerVisible = false;

  Future<void> loginUserAndRoute({required String email, required String pwd}) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: pwd,
      );
      if (userCredential.user != null) {
        Navigator.pushNamed(context, ChatScreen.routeId);
      } else {
        print('Something went wrong');
      }
    } catch (e) {
      print('LoginScreen.loginUserAndRoute caught an error!\t==>\n$e');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kMainColor,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Flexible(
                child: Hero(
                  tag: kHeroLogoTag,
                  child: SizedBox(
                    height: 200.0,
                    child: Image.asset('images/logo.png'),
                  ),
                ),
              ),
              const SizedBox(height: 40.0),
              TextField(
                keyboardType: TextInputType.emailAddress,
                textAlign: TextAlign.center,
                style: kInputTextFieldTextStyle,
                onChanged: (value) {
                  email = value;
                },
                decoration: kInputTextFieldDecoration.copyWith(hintText: 'Enter your email'),
              ),
              const SizedBox(height: 8.0),
              TextField(
                obscureText: true,
                textAlign: TextAlign.center,
                style: kInputTextFieldTextStyle,
                onChanged: (value) {
                  password = value;
                },
                decoration: kInputTextFieldDecoration.copyWith(hintText: 'Enter your password'),
              ),
              const SizedBox(height: 24.0),
              RoundedButton(
                title: 'Log In',
                color: kMainAccentColor,
                onPressed: () async {
                  setState(() {
                    isSpinnerVisible = true;
                  });
                  await loginUserAndRoute(email: email, pwd: password);
                  setState(() {
                    isSpinnerVisible = false;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
