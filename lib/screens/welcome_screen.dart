import 'dart:math' as math;

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flash_chat/screens/login_screen.dart';
import 'package:flutter_flash_chat/screens/registration_screen.dart';
import 'package:flutter_flash_chat/util/constants.dart';
import 'package:flutter_flash_chat/widgets/rounded_button.dart';

class WelcomeScreen extends StatefulWidget {
  static const String routeId = 'welcome_screen';

  const WelcomeScreen({Key? key}) : super(key: key);
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController animationController;
  late Animation animation;

  @override
  void initState() {
    super.initState();

    animationController = AnimationController(
      duration: const Duration(seconds: 2),
      vsync: this,
      // upperBound: 1.0,
    );

    animationController.forward();
    animation = ColorTween(
      begin: null,
      end: kMainColor,
    ).animate(animationController);

    animationController.addListener(() {
      setState(() {});
      // print(animation.value);
    });
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation.value,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: <Widget>[
                Hero(
                  tag: kHeroLogoTag,
                  child: SizedBox(
                    // child: Image.asset('images/logo.png'),
                    child: buildTweenAnimationLogo(),
                    height: 60.0,
                  ),
                ),
                SizedBox(
                  width: 250.0,
                  child: buildAnimatedLeadText(),
                ),
              ],
            ),
            const SizedBox(
              height: 100.0,
            ),
            RoundedButton(
              color: kMainAccentColor,
              title: 'Log In',
              onPressed: () =>
                  Navigator.pushNamed(context, LoginScreen.routeId),
            ),
            RoundedButton(
              color: kSecondaryAccentColor,
              title: 'Register',
              onPressed: () =>
                  Navigator.pushNamed(context, RegistrationScreen.routeId),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildAnimatedLeadText() {
    return AnimatedTextKit(
      totalRepeatCount: 1,
      animatedTexts: [
        TypewriterAnimatedText(
          'Flash Chat',
          textStyle: kLeadTextStyle,
          curve: Curves.easeOut,
          speed: const Duration(milliseconds: 150),
        ),
      ],
    );
  }

  Widget buildTweenAnimationLogo() {
    return TweenAnimationBuilder(
        tween: Tween<double>(begin: 0, end: 1 * math.pi),
        duration: const Duration(seconds: 2),
        builder: (_, double angle, __) {
          return Transform.rotate(
            angle: angle,
            child: Image.asset('images/logo.png'),
          );
        });
  }
}
