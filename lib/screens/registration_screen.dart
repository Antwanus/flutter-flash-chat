import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flash_chat/screens/chat_screen.dart';
import 'package:flutter_flash_chat/util/constants.dart';
import 'package:flutter_flash_chat/widgets/rounded_button.dart';
import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';

class RegistrationScreen extends StatefulWidget {
  static const String routeId = 'register';

  const RegistrationScreen({Key? key}) : super(key: key);
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  late String email;
  late String password;
  bool isSpinnerVisible = false;

  Future<void> createUserAndRoute({required String login, required String pwd}) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: login,
        password: pwd,
      );
      if (userCredential.user != null) {
        Navigator.pushNamed(context, ChatScreen.routeId);
      } else {
        print('Something went wrong');
      }
    } catch (e) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('$e'),
        backgroundColor: Theme.of(context).shadowColor,
        duration: const Duration(seconds: 10),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomInset: false,
      backgroundColor: kMainColor,
      body: ModalProgressHUD(
        inAsyncCall: isSpinnerVisible,
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                  child: Hero(
                    tag: kHeroLogoTag,
                    child: SizedBox(
                      height: 200.0,
                      child: Image.asset('images/logo.png'),
                    ),
                  ),
                ),
                const SizedBox(height: 48.0),
                TextField(
                  keyboardType: TextInputType.emailAddress,
                  textAlign: TextAlign.center,
                  style: kInputTextFieldTextStyle,
                  onChanged: (value) {
                    email = value;
                  },
                  decoration: kInputTextFieldDecoration.copyWith(
                    hintText: 'Enter your email',
                  ),
                ),
                const SizedBox(height: 8.0),
                TextField(
                  obscureText: true,
                  textAlign: TextAlign.center,
                  style: kInputTextFieldTextStyle,
                  onChanged: (value) {
                    password = value;
                  },
                  decoration: kInputTextFieldDecoration.copyWith(
                    hintText: 'Enter your password',
                  ),
                ),
                const SizedBox(height: 24.0),
                RoundedButton(
                  title: 'Register',
                  color: kSecondaryAccentColor,
                  onPressed: () async {
                    setState(() {
                      isSpinnerVisible = true;
                    });

                    print('$email - $password');
                    await createUserAndRoute(login: email, pwd: password);
                    setState(() {
                      isSpinnerVisible = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
