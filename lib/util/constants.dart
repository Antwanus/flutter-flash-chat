import 'package:flutter/material.dart';

const kMainColor = Color.fromRGBO(235, 83, 83, 1.0);
const kSecondaryColor = Color.fromRGBO(230, 191, 6, 1.0);
const kMainAccentColor = Color.fromRGBO(54, 174, 124, 1.0);
const kSecondaryAccentColor = Color.fromRGBO(24, 116, 152, 1.0);

const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const kMessageTextFieldDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  hintText: 'Type your message here...',
  border: InputBorder.none,
);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);

const kLeadTextStyle = TextStyle(
    fontSize: 33.0,
    fontFamily: 'SourceCodePro',
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.italic,
    letterSpacing: 2.0,
    color: kSecondaryColor
// color: Colors.pink,
    );

const String kHeroLogoTag = 'hero_logo_tag';
const kInputTextFieldDecoration = InputDecoration(
    hintText: '',
    hintStyle: TextStyle(color: Colors.white60, fontStyle: FontStyle.italic),
    contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(color: kSecondaryColor, width: 1.0),
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(color: kMainAccentColor, width: 2.0),
      borderRadius: BorderRadius.all(Radius.circular(32.0)),
    ));

const kInputTextFieldTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 20,
  fontFamily: 'UbuntuMono',
);
